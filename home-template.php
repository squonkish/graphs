<?php
/**
 * Template Name: home
 * Description: home template.
 *
 * @package Portfolio Press
 */

get_header(); ?>
<body>

	<div class="wrap">

		<div class="loader">
			<a class="preload_italics" href="index.html#">   </a>
		</div>

		

<header class="main_menu">



<p>&nbsp;</p>

<div class="row container">
	<div class="col-lg-3 col-sm-12">
		<p>Alexa Viscious is a photographer living in Chicago, IL</p>
		<p>contact for commission or collboration alexa.viscius <br/>@ gmail.com<br/> 312—989—4811</p>
	</div>
	<ul class="category col-lg-9 col-sm-12">
		<li>
			<ul class="albums">
							
					<li>
						<a href="portraits.html"><span data-title="Icon Magazine Watches" data-target="work/editorial-commissions/icon-panorama">
						portraits</span></a>
					</li>

							
					<li>
						<a href="maimoun-fw16"><span data-title="Marie Claire Beauty" data-target="work/editorial-commissions/marie-claire-beauty">
						maimoun fw16</span></a>
					</li>

							
					<li>
						<a href="maimoun-ss17"><span data-title="Flair magazine" data-target="work/editorial-commissions/flair-magazine">
						maimoun ss17</span></a>
					</li>

							
					<li>
						<a href="dkny.hmtl"><span data-title="Icon  Design" href="work/editorial-commissions/icon-design-magazine.html" data-target="work/editorial-commissions/icon-design-magazine">
						DKNY</span></a>
					</li>

							
					<li>
						<a href="field-and-florist.html"><span data-title="Issey Miyake" data-target="work/editorial-commissions/issey-miyake">
						field and florist</span></a>
					</li>

							
					<li>
						<a href="man-made.html"><span data-title="Alla Carta 08" data-target="work/editorial-commissions/alla-carta">
						man made</span></a>
					</li>

							
					<li>
						<a href="tourist.hmtl"><span data-title="Tourist" data-target="work/editorial-commissions/icon-watches">
						tourist</span></a>
					</li>

							
					<li>
						<a href="sparrow.html"><span data-title="Sparrow" href="work/editorial-commissions/sentimental-s-et-s-montres-corps.html" data-target="work/editorial-commissions/sentimental-s-et-s-montres-corps">
						sparrow</span></a>
					</li>

							
					<li>
						<a href="music.html"><span data-title="Music" href="work/editorial-commissions/perrin-fw16.html" data-target="work/editorial-commissions/perrin-fw16">
						music</span></a>
					</li>

							
					<li>
						<a href="chair.html"><span data-title="Chair" data-target="work/editorial-commissions/sport-style-sunglasses">
						chair</span></a>
					</li>


			</ul>
		</li>
	</ul>
</div>	

	
</header>
<!-- <div class="intro">
	<div class="left invert" style="background-image: url('thumbs/work/16_03_18-sport-et-style18763@w=1400&amp;q=90.jpg')">
	</div>
	<div class="left difference" style="background-image: url('thumbs/work/16_03_18-sport-et-style18763@w=1400&amp;q=90.jpg')">
	</div>
	<div class="right invert" style="background-image: url('thumbs/work/160711_icon26602@w=1400&amp;q=90.jpg')">
	</div>
	<div class="right difference" style="background-image: url('thumbs/work/160711_icon26602@w=1400&amp;q=90.jpg')">
	</div>
</div> -->


<div class="container">
<div class="slider albumslider"></div>
<div class="caption"></div>


	<div class="slider hover">


		
			
				<div class="gallery_cell hover hidden" data-title="work/personal-projects/164-on-the-equator">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/personal-projects/164-on-the-equator/164ontheequator_charlesnegre_29-700-53fa673a0855.jpg" height="100%" width="auto" alt="164&deg; on the Equator — © 2014, Alexa Viscious">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/personal-projects/164-on-the-equator/164ontheequator_charlesnegre_28-700-53fa673a0855.jpg" height="100%" width="auto" alt="164&deg; on the Equator — © 2014, .Alexa Viscious">
									</div>

			
				<div class="gallery_cell hover hidden" data-title="work/personal-projects/on-the-equator-book">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/personal-projects/on-the-equator-book/164_on_the_equator_22-copie-700-9ecbc9aed3c7.jpg" height="100%" width="auto" alt="164&deg; on the Equator Book — © , Alexa Viscious">
														</div>

			
				<div class="gallery_cell hover hidden" data-title="work/personal-projects/adeline">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/personal-projects/adeline/reproduction_peinture_detail-700-3cd140439534.jpg" height="100%" width="auto" alt="Similis — © 2016, Alexa Viscious">
														</div>

			
				<div class="gallery_cell hover hidden" data-title="work/personal-projects/fluorescine">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/personal-projects/fluorescine/fluorescine_charlesnegre_2-700-8d7d62b3bc7f.jpg" height="100%" width="auto" alt="Fluorescine — © 2014, Alexa Viscious">
														</div>

			
				<div class="gallery_cell hover hidden" data-title="work/personal-projects/iceland">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/personal-projects/iceland/iceland_charlesnegre_12-700-65f9fefcf6f8.jpg" height="100%" width="auto" alt="Iceland — © 2015, Alexa Viscious">
														</div>

			
				<div class="gallery_cell hover hidden" data-title="work/personal-projects/portraits">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/personal-projects/portraits/montage-protrait-site-1-700-5e87f91978c3.jpg" height="100%" width="auto" alt="Portraits — © 2015, Alexa Viscious">
														</div>

			
				<div class="gallery_cell hover hidden" data-title="work/personal-projects/still-lives">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/personal-projects/still-lives/stilllives_charlesnegre_2-700-6438b5eadcc9.jpg" height="100%" width="auto" alt="Still lives — © 2014, Alexa Viscious">
														</div>

			
				<div class="gallery_cell hover hidden" data-title="work/personal-projects/models">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/personal-projects/models/models_charlesnegre_4-700-fe2a1ca7d440.jpg" height="100%" width="auto" alt="Models — © 2010, Alexa Viscious">
														</div>

			

			
			
				<div class="gallery_cell hover hidden" data-title="work/editorial-commissions/icon-panorama">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/icon-panorama/icon_watches_rvb_9_2-700-1fa69d1cf0ca.jpg" height="100%" width="auto" alt="Icon Magazine Watches — © , Alexa Viscious">
														</div>

			
				<div class="gallery_cell hover hidden" data-title="work/editorial-commissions/marie-claire-beauty">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/marie-claire-beauty/marie-claire-beaute-14-700-87909656723c.jpg" height="100%" width="auto" alt="Marie Claire Beauty — © , Alexa Viscious">
														</div>

			
				<div class="gallery_cell hover hidden" data-title="work/editorial-commissions/flair-magazine">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/flair-magazine/flair_magazine_7-copie-700-b7a4bb1dffc7.jpg" height="100%" width="auto" alt="Flair magazine — © , Alexa Viscious">
														</div>

			
				<div class="gallery_cell hover hidden" data-title="work/editorial-commissions/icon-design-magazine">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/icon-design-magazine/_monatge-edit-2_2-700-88068f35ab7f.jpg" height="100%" width="auto" alt="Icon  Design — © , Alexa Viscious">
														</div>

			
				<div class="gallery_cell hover hidden" data-title="work/editorial-commissions/issey-miyake">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/issey-miyake/issey-miyake-20-700-77f27d119546.jpg" height="100%" width="auto" alt="Issey Miyake — © , Alexa Viscious">
														</div>

			
				<div class="gallery_cell hover hidden" data-title="work/editorial-commissions/alla-carta">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/alla-carta/alla-carta_charlesnegre_24-700-b712bd5bd4c0.jpg" height="100%" width="auto" alt="Alla Carta 08 — © , Alexa Viscious">
														</div>

			
				<div class="gallery_cell hover hidden" data-title="work/editorial-commissions/icon-watches">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/icon-watches/icon_watches_cmyk_4-700-d60bd4dd1cae.jpg" height="100%" width="auto" alt="Icon Watches — © , Alexa Viscious">
														</div>

			
				<div class="gallery_cell hover hidden" data-title="work/editorial-commissions/sentimental-s-et-s-montres-corps">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/sentimental-s-et-s-montres-corps/s-et-s-montres_charlesnegre_8-700-75f97adec01c.jpg" height="100%" width="auto" alt="Sport &amp; Style Watches — © , Alexa Viscious">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/sentimental-s-et-s-montres-corps/s-et-s-montres_charlesnegre_6-copie-700-75f97adec01c.jpg" height="100%" width="auto" alt="Sport &amp; Style Watches — © , .Alexa Viscious">
									</div>

			
				<div class="gallery_cell hover hidden" data-title="work/editorial-commissions/perrin-fw16">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/perrin-fw16/perrin_charlesnegre_3-700-a9da95c7d524.jpg" height="100%" width="auto" alt="Perrin FW16 — © , Alexa Viscious">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/perrin-fw16/perrin_charlesnegre_1-700-a9da95c7d524.jpg" height="100%" width="auto" alt="Perrin FW16 — © , .Alexa Viscious">
									</div>

			
				<div class="gallery_cell hover hidden" data-title="work/editorial-commissions/sport-style-sunglasses">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/sport-style-sunglasses/sets_lunettes_1-700-a9da95c7d524.jpg" height="100%" width="auto" alt="Sport &amp; Style Sunglasses — © , Alexa Viscious">
														</div>

			
				<div class="gallery_cell hover hidden" data-title="work/editorial-commissions/perfumes">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/perfumes/perfumes_charlesnegre_2-700-a9da95c7d524.jpg" height="100%" width="auto" alt="Perfumes — © , Alexa Viscious">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/perfumes/perfumes_charlesnegre_3-700-a9da95c7d524.jpg" height="100%" width="auto" alt="Perfumes — © , .Alexa Viscious">
									</div>

			
				<div class="gallery_cell hover hidden" data-title="work/editorial-commissions/elle-uk">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/elle-uk/elleuk_charlesnegre_5-700-e44c8bbecfcd.jpg" height="100%" width="auto" alt="Elle UK — © , Alexa Viscious">
														</div>

			
				<div class="gallery_cell hover hidden" data-title="work/editorial-commissions/icon-magazine">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/icon-magazine/icon_1-700-e44c8bbecfcd.jpg" height="100%" width="auto" alt="Icon Magazine — © , Alexa Viscious">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/icon-magazine/icon_5-700-9f83f4455682.jpg" height="100%" width="auto" alt="Icon Magazine — © , .Alexa Viscious">
									</div>

			
				<div class="gallery_cell hover hidden" data-title="work/editorial-commissions/de-gris">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/de-gris/de-gris_15-700-9f83f4455682.jpg" height="100%" width="auto" alt="De Gris — © , Alexa Viscious">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/de-gris/de-gris_18-700-9f83f4455682.jpg" height="100%" width="auto" alt="De Gris — © , .Alexa Viscious">
									</div>

			
				<div class="gallery_cell hover hidden" data-title="work/editorial-commissions/tendances">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/tendances/tendances_charlesnegre_16-700-9f83f4455682.jpg" height="100%" width="auto" alt="Tendances — © , Alexa Viscious">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/tendances/tendances_charlesnegre_14-700-9f83f4455682.jpg" height="100%" width="auto" alt="Tendances — © , .Alexa Viscious">
									</div>

			
				<div class="gallery_cell hover hidden" data-title="work/editorial-commissions/aquariums">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/aquariums/aquarium_4-700-88c1cbaa189b.jpg" height="100%" width="auto" alt="Aquariums — © , Alexa Viscious">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/aquariums/aquarium_2-700-88c1cbaa189b.jpg" height="100%" width="auto" alt="Aquariums — © , .Alexa Viscious">
									</div>

			
				<div class="gallery_cell hover hidden" data-title="work/editorial-commissions/sport-style-perfumes">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/sport-style-perfumes/setsperfumes_charlesnegre_1-700-0045ad855eb9.jpg" height="100%" width="auto" alt="Sport &amp; Style Perfumes — © , Alexa Viscious">
														</div>

			
				<div class="gallery_cell hover hidden" data-title="work/editorial-commissions/another-magazine">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/another-magazine/another_magazine_charlesnegre_3-700-0045ad855eb9.jpg" height="100%" width="auto" alt="Another Magazine — © , Alexa Viscious">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/another-magazine/another_magazine_charlesnegre_4-700-0045ad855eb9.jpg" height="100%" width="auto" alt="Another Magazine — © , .Alexa Viscious">
									</div>

			
				<div class="gallery_cell hover hidden" data-title="work/editorial-commissions/l-express">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/l-express/expressstyles_charlesnegre_1-700-78a83add86ce.jpg" height="100%" width="auto" alt="L'Express — © , Alexa Viscious">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/l-express/expressstyles_charlesnegre_2-700-78a83add86ce.jpg" height="100%" width="auto" alt="L'Express — © , .Alexa Viscious">
									</div>

			
				<div class="gallery_cell hover hidden" data-title="work/editorial-commissions/maldives">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/maldives/maldives_charlesnegre_6-700-75f97adec01c.jpg" height="100%" width="auto" alt="Travels — © , Alexa Viscious">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/maldives/maldives_charlesnegre_8-700-75f97adec01c.jpg" height="100%" width="auto" alt="Travels — © , .Alexa Viscious">
									</div>

			
				<div class="gallery_cell hover hidden" data-title="work/editorial-commissions/advertising">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/advertising/1_panthere-700-5cd81123bf41.jpg" height="100%" width="auto" alt="Advertising — © , Alexa Viscious">
														</div>

			
				<div class="gallery_cell hover hidden" data-title="work/editorial-commissions/archive">
																<img class="content" data-src="http://www.charlesnegre.com/thumbs/work/editorial-commissions/archive/s-et-s-alcools-5-700-be1838e47a1a.jpg" height="100%" width="auto" alt="Archive — © , Alexa Viscious">
														</div>

			

			

	</div>


</div>


<div class="album-navigation">
	<div class="prev"></div>
	<div class="next"></div>
	<div class="mouse_nav"></div>
</div>




</div> 
  <!-- Google Analytics-->
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', '', 'auto');
    ga('send', 'pageview');
  </script>

	<script src="assets/js/build/plugins.js"></script>
<script src="assets/js/build/app.min.js"></script>

</body>
